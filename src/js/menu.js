let activeMenu = document.getElementsByClassName('sidebar__container-box');
console.log(activeMenu)
for (let i = 0; i < activeMenu.length; i++) {
  activeMenu[i].onclick = function () {
    let elem = activeMenu[0];
    while (elem) {
      elem.classList.remove('active');
      elem = elem.nextElementSibling;
    }
    this.classList.add("active");
  }
}

function classToggle() {
  //Icons
  const navs = document.querySelectorAll('.sidebar__container-box--hide')
  const navbox = document.querySelectorAll('.sidebar__container-box')
  const navboxmargin = document.querySelectorAll('.sidebar__container-box')
  const navlink = document.querySelectorAll('.nav__link')

  navboxmargin.forEach(nav => nav.classList.toggle('nav--padding'));
  navbox.forEach(nav => nav.classList.toggle('nav__toggleShow'));
  navs.forEach(nav => nav.classList.toggle('nav__toggleShow'));
  navlink.forEach(nav => nav.classList.toggle('nav__toggleShow'));

  //Sidebar
  const navShow = document.querySelectorAll('.sidebar')
  navShow.forEach(sidebar => sidebar.classList.toggle('sidebar--show'));
  const iconBurger = document.querySelectorAll('.sidebar__menuicon-burger')
  iconBurger.forEach(sidebar => sidebar.classList.toggle('sidebar__burger-icon'));
  const logoShow = document.querySelectorAll('.logo-show')
  logoShow.forEach(sidebar => sidebar.classList.toggle('logo__show'));

}
document.querySelector('.sidebar__menuicon')
  .addEventListener('click', classToggle);
